FROM maven:3.6.3-jdk-8
# install deps
RUN apt-get update && apt-get -y install libglib2.0-dev libxi6 libnss3-dev apt-utils
# Install Chrome
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN dpkg -i google-chrome-stable_current_amd64.deb; apt-get -fy install
# install chromedriver
RUN apt-get install -yqq unzip
RUN wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip
RUN unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/
# set display port to avoid crash
ENV DISPLAY=:99
WORKDIR /app/test
COPY . .
ENTRYPOINT [ "mvn", "-Dtest=testRunner", "test" ]