 FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch

 # Install Chrome
 RUN apt-get update && apt-get install -y \
 apt-transport-https \
 ca-certificates \
 curl \
 gnupg \
 hicolor-icon-theme \
 libcanberra-gtk* \
 libgl1-mesa-dri \
 libgl1-mesa-glx \
 libpango1.0-0 \
 libpulse0 \
 libv4l-0 \
 fonts-symbola \
 --no-install-recommends \
 && curl -sSL https://dl.google.com/linux/linux_signing_key.pub | apt-key add - \
 && echo "deb [arch=amd64] https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list \
 && apt-get update && apt-get install -y \
 google-chrome-stable \
 --no-install-recommends \
 && apt-get purge --auto-remove -y curl \
 && rm -rf /var/lib/apt/lists/*

FROM mcr.microsoft.com/dotnet/core/sdk:3.1.301-alpine AS build-env

WORKDIR /app

# Copy csproj and restore as distinct layers
COPY DockerDotnetApp/DockerDotnetApp.csproj ./


RUN dotnet restore
#RUN dotnet restore DockerDotnetApp/DockerDotnetApp.csproj

COPY . ./

RUN dotnet publish DockerDotnetApp/DockerDotnetApp.csproj -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1.5-alpine

WORKDIR /app
COPY --from=build-env /app/out/ .
EXPOSE 80
ENTRYPOINT ["dotnet", "DockerDotnetApp.dll"]

FROM mcr.microsoft.com/dotnet/core/sdk:3.1.301-alpine AS build-env

WORKDIR /app

# Copy csproj and restore as distinct layers
COPY DockerDotnetApp/DockerDotnetApp.csproj ./


RUN dotnet restore
#RUN dotnet restore DockerDotnetApp/DockerDotnetApp.csproj

COPY . ./

RUN dotnet publish DockerDotnetApp/DockerDotnetApp.csproj -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1.5-alpine

WORKDIR /app
COPY --from=build-env /app/out/ .
EXPOSE 80
ENTRYPOINT ["dotnet", "DockerDotnetApp.dll"]



# BUILD STAGE
FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS build
WORKDIR /uitests
COPY . .
RUN dotnet restore
RUN dotnet build
ENTRYPOINT ["dotnet", "test", "UITests.csproj"]
