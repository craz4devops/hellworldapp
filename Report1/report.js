$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:features/homePage.feature");
formatter.feature({
  "name": "Demo home page scenarios",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Verify there are 3 tabs on the sidebar",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@P1"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I am on Demo Home Page",
  "keyword": "Given "
});
formatter.match({
  "location": "steps.homePage_Steps.i_am_on_Demo_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I check links on the sidebar",
  "keyword": "When "
});
formatter.match({
  "location": "steps.homePage_Steps.i_check_links_on_the_sidebar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "All the tabs displayed correctly",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.homePage_Steps.all_the_tabs_displayed_correctly()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});