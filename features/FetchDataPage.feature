Feature: Fetch Data page scenarios

  @P5
  Scenario Outline: Title of your scenario outline
    Given I am on Fetch Data Page
    When I check weather by the '<date>'
    Then I validate the '<TempC>', '<TempF>', '<Summary>' as expected

    Examples: 
      | date       | TempC | TempF | Summary |
      | 08/02/2020 |    18 |    64 | Chilly  |
      | 08/03/2020 |   -10 |    15 | Bracing |
      | 08/04/2020 |    13 |    55 | Chilly  |
      | 08/05/2020 |    25 |    76 | Balmy   |
      | 08/06/2020 |    13 |    55 | Bracing |
