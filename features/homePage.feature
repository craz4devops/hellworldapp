
Feature: Demo home page scenarios
	@P1
  Scenario: Verify there are 3 tabs on the sidebar
    Given I am on Demo Home Page
    When I check links on the sidebar
    Then All the tabs displayed correctly
	  
	@P2
  Scenario: Verify Counter Page displayed
    Given I am on Demo Home Page
    When I click on Counter text
    Then I am navigated to counter page

	@P3
  Scenario: Verify Fetch data Page displayed
    Given I am on Demo Home Page
    When I click on Fetch data text
    Then I am navigated to fetchdata page

	@P4
	Scenario: Verify survay monkey displayed
    Given I am on Demo Home Page
    When I click on brief survey
    Then I am navigated to surveymonkey