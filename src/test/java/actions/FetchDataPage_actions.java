package actions;

import org.openqa.selenium.WebDriver;

import elements.FetchDataPage_Elements;
import steps.Common_Steps;


public class FetchDataPage_actions {
	private WebDriver driver;
	FetchDataPage_Elements fatchdatapage_elements;
	
	public FetchDataPage_actions(Common_Steps common_steps) {
		this.driver =common_steps.getDriver();
		fatchdatapage_elements = new FetchDataPage_Elements(driver);
		
	}
	
	public int getSizeDataRow () {
		return fatchdatapage_elements.dateRow.size();
	}
	
	public String getTempCAct (int r) {
		return fatchdatapage_elements.TempCAct.get(r).getText();
	}
	
	public String getTempFAct (int r) {
		return fatchdatapage_elements.TempFAct.get(r).getText();
	}
	
	public String getSummaryAct (int r) {
		return fatchdatapage_elements.SummaryAct.get(r).getText();
	}
	
	public boolean conditiondateRow(int r, String string) {
		return fatchdatapage_elements.dateRow.get(r).getText().contains(string);
	}
}
