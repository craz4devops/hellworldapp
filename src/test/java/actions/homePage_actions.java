package actions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import elements.homePage_Elements;
import steps.Common_Steps;

public class homePage_actions {
	
	private WebDriver driver;
	homePage_Elements homepage_elements;
	
	public homePage_actions(Common_Steps common_steps) {
		this.driver =common_steps.getDriver();
		homepage_elements = new homePage_Elements(driver);
		
	}
	public List<WebElement> listofLinks() {
		return homepage_elements.sidebar.findElements(By.tagName("a"));
	}

	public void clickCounterText() {
		homepage_elements.CounterText.click();
	}
	
	public void clickFetchData() {
		homepage_elements.FetchData.click();
	}
	
	public void clickbriefSurvey() {
		homepage_elements.briefSurvey.click();
	}
}
