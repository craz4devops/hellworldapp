package steps;

import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import actions.FetchDataPage_actions;
import actions.common_Actions;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class FetchDataPage_Steps {
	

	common_Actions common_actions;
	FetchDataPage_actions fetchdatapage_actions;
	
	String TempCAct;
	String TempFAct;
	String SummaryAct;
	
	public FetchDataPage_Steps(common_Actions common_actions,FetchDataPage_actions fetchdatapage_actions) {
		this.common_actions =common_actions;
		this.fetchdatapage_actions=fetchdatapage_actions;
		
	}

	@Given("I am on Fetch Data Page")
	public void i_am_on_Fetch_Data_Page() {
		
		common_actions.goToUrl("http://3.129.38.180:8080/fetchdata");
		String expURL = "http://3.129.38.180:8080/fetchdata";
		String actURL = common_actions.getCurrentPageUrl();
		if (!expURL.equals(actURL)) {
			fail("Page does not navigate to expected URL");
		}
	    
	}

	@When("I check weather by the {string}")
	public void i_check_weather_by_the(String string) {
		
		int rows=fetchdatapage_actions.getSizeDataRow();
		
		for(int r=1; r<=rows; r++) {
			if (fetchdatapage_actions.conditiondateRow(r, string)) {
			TempCAct =	fetchdatapage_actions.getTempCAct(r);
			TempFAct =	fetchdatapage_actions.getTempFAct(r);
			SummaryAct =fetchdatapage_actions.getSummaryAct(r);
				
			}
			
		}
		
	   
	}

	@Then("I validate the {string}, {string}, {string} as expected")
	public void i_validate_the_as_expected(String TempC, String TempF, String Summary) {
	   if(TempCAct.contains(TempC)) {
		   fail("Temp. (C) mismatch");
	   }
	   if(TempFAct.contains(TempF)) {
		   fail("Temp. (F) mismatch");
	   }
	   if(SummaryAct.contains(Summary)) {
		   fail("Summary mismatch");
	   }
	}
}
