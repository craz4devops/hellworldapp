package steps;
import java.util.concurrent.TimeUnit;
import java.net.URL;
import java.net.MalformedURLException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class Common_Steps {

	private WebDriver driver;
	URL url;
	 @Before
	 public void setUp() {
			 //System.setProperty("webdriver.chrome.driver", "C://downloads//chromedriver.exe");
			 System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
			 try {
				 		String u = "http://siraparapu3c.mylabserver.com:9090/wd/hub";
				 		url = new URL(u);
			 	 } 		catch (MalformedURLException e) 
			 	 {
			 		 	e.printStackTrace();
			 	 }
				ChromeOptions chromeOptions = new ChromeOptions();
			 	//chromeOptions.addArguments("--headless");
				//chromeOptions.addArguments("--no-sandbox");
			 	//chromeOptions.addArguments("test-type");
			 	//chromeOptions.addArguments("start-maximized");
			 	//chromeOptions.addArguments("--window-size=1920,1080");
			 	//chromeOptions.addArguments("--enable-precise-memory-info");
			 	//chromeOptions.addArguments("--disable-popup-blocking");
			 	//chromeOptions.addArguments("--disable-default-apps");
			 	//chromeOptions.addArguments("test-type=browser");
				driver = new RemoteWebDriver(url,chromeOptions);
				driver.manage().deleteAllCookies();
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				driver.manage().window().maximize();
	 }
	
	@After
	public void tearDown() {
		driver.quit();
	}
	
	public WebDriver getDriver() {
		return driver;
	}
}
