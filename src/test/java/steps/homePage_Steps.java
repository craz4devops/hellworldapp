package steps;

import static org.junit.Assert.fail;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import actions.common_Actions;
import actions.homePage_actions;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class homePage_Steps {
	
	common_Actions common_actions;
	homePage_actions homepage_actions;
	
	
	public homePage_Steps(common_Actions common_actions,homePage_actions homepage_actions) {
		this.common_actions =common_actions;
		this.homepage_actions = homepage_actions;
		
	}
	
	@Given("I am on Demo Home Page")
	public void i_am_on_Demo_Home_Page() {
		
		//driver.get("http://3.129.38.180:8080/");
		common_actions.goToUrl("http://3.129.38.180:8080/");
		String expURL = "http://3.129.38.180:8080/";
		String actURL = common_actions.getCurrentPageUrl();
				//driver.getCurrentUrl();
		if (!expURL.equals(actURL)) {
			fail("Page does not navigate to expected URL");
		}
		
	}

	@When("I check links on the sidebar")
	public void i_check_links_on_the_sidebar() {
		//WebElement sidebar = driver.findElement(By.xpath("//ul[@class='nav flex-column']"));
		List <WebElement> x= homepage_actions.listofLinks();
	    System.out.println("There are : "+x.size()+"tabs");
	    for(WebElement links:x) {
	    	System.out.println(links.getText());
	    }
		
	}

	@Then("All the tabs displayed correctly")
	public void all_the_tabs_displayed_correctly() {
		
		
	}
	
	@When("I click on Counter text")
	public void i_click_on_Counter_text() {
		homepage_actions.clickCounterText();
	 
	}

	@Then("I am navigated to counter page")
	public void i_am_navigated_to_counter_page() {
		
		String expURL = "http://3.129.38.180:8080/counter";
		String actURL = common_actions.getCurrentPageUrl();
		if (!expURL.equals(actURL)) {
			fail("Page does not navigate to expected URL");
		}
	  
	}

	@When("I click on Fetch data text")
	public void i_click_on_Fetch_data_text() {
		homepage_actions.clickFetchData();
	}

	@Then("I am navigated to fetchdata page")
	public void i_am_navigated_to_fetchdata_page() {
		
		String expURL = "http://3.129.38.180:8080/fetchdata";
		String actURL = common_actions.getCurrentPageUrl();
		if (!expURL.equals(actURL)) {
			fail("Page does not navigate to expected URL");
		}
		
	}

	@When("I click on brief survey")
	public void i_click_on_brief_survey() {
		homepage_actions.clickbriefSurvey();
	}

	@Then("I am navigated to surveymonkey")
	public void i_am_navigated_to_surveymonkey() {
	    
		String expURL = "https://www.surveymonkey.com/r/S6QBFQN";
		String actURL = common_actions.getCurrentPageUrl();
		if (!expURL.equals(actURL)) {
			fail("Page does not navigate to expected URL");
		}
		
	}

}
