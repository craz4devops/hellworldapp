package elements;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FetchDataPage_Elements {
	
	WebDriver driver;

	@FindBy(xpath= "//body//tbody//tr") public List<WebElement> dateRow;
	@FindBy(xpath= "//tr[*]/td[2]") public List<WebElement> TempCAct;
	@FindBy(xpath= "//tr[*]/td[3]") public List<WebElement> TempFAct;
	@FindBy(xpath= "//tr[*]/td[4]") public List<WebElement> SummaryAct;
	
	public FetchDataPage_Elements(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

}
