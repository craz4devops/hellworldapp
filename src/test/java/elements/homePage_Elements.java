package elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class homePage_Elements {
	
	WebDriver driver;

	@FindBy(xpath= "//ul[@class='nav flex-column']") public WebElement sidebar;
	
	@FindBy(xpath= "//li[2]//a[1]") public WebElement CounterText;
	
	@FindBy(xpath= "//li[3]//a[1]") public WebElement FetchData;
	
	@FindBy(xpath= "//a[@class='font-weight-bold']") public WebElement briefSurvey;
	
	public homePage_Elements(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
}
